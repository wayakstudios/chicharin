-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

local configs 	= require("configs.config");
local storage 	= require("extras.data");
appodeal_setApiKey("7f56d4a77fff84043d6e7d2d433bdf73d525afd74ad645a9");
appodeal_initialize();
initialize_storage();

local logo      = getSprite("logo.png",{layer="main",r=true,x=0,y=0,w=700,h=700});
local closure   = function()
    display.remove(logo);
    local core	= require("system.game");
end
transition.to(logo,{time=2000,alpha=0,onComplete=closure});