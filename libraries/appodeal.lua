local appodeal  = require( "plugin.appodeal" )
local adds      = {};
local config    = {};
local ready     = false;
local attemps   = 0;
local apiKey    = "";
local currentAdd= "";    

function appodeal_adListener( event )
    if ( event.phase == "init" ) then  -- Successful initialization
         appodeal.load( config.type )
         ready = true;
         appodeal_show();
    else
         print("not loaded"..config.type)
         print( event.isError )
    end
end

function appodeal_setApiKey(ak)
    apiKey = ak;
end

function appodeal_initialize(params)
    params                  = validate(params,{});
    config                  = {};
    config.testMode         = validate(params.test,true);
    config.smartBanners     = validate(params.smart,true);
    config.supportedAdTypes = validate(params.types,{"banner","interstitial"})
    config.appKey           = apiKey;
    appodeal.init( appodeal_adListener, config )
    -- appodeal.init( appodeal_adListener, { appKey="7f56d4a77fff84043d6e7d2d433bdf73d525afd74ad645a9",testMode=true,smartBanners=true,supportedAdTypes={"banner","interstitial"} } )
end

function appodeal_setAdd(option)
    local adds = {"top_banner","bottom_banner","interstitial"};
    currentAdd = adds[option];
end

function appodeal_hide()
    addType = "";
    if(currentAdd == "top_banner" or currentAdd == "bottom_banner") then
        addType = "banner";
    else
        addType = "interstitial";
    end
    appodeal.hide( addType );
end

function appodeal_show()
    if(ready) then
        addType = "";
        if(currentAdd == "top_banner" or currentAdd == "bottom_banner") then
            addType = "banner";
        else
            addType = "interstitial";
        end
        if appodeal.isLoaded(addType) then
            if(currentAdd == "top_banner") then
                appodeal.show( addType, { yAlign="top" } )
            elseif(currentAdd == "bottom_banner") then
                appodeal.show( addType, { yAlign="bottom" } )
            else
                appodeal.show(addType);
            end
        end
    else
        if(attemps < 10 ) then
            attemps = attemps + 1;
            timer.performWithDelay( 5000, appodeal_show,1 );
        end
    end
end

--[[function appodeal_Initialize(params)
    config = params;
    appodeal.init( appodeal_adListener, { appKey="7f56d4a77fff84043d6e7d2d433bdf73d525afd74ad645a9",testMode=true,smartBanners=true,supportedAdTypes={"banner","interstitial"} } )
end

function appodeal_hide(params)
    appodeal.hide( config.type )
end

-- Initialize the Appodeal plugin
function appodeal_show()
    if(ready) then
        if appodeal.isLoaded(config.type) then
            if(config.type == "banner") then
                appodeal.show( "banner", { yAlign="bottom" } )
            else
                appodeal.show(config.type);
            end
        end
    else
        if(attemps < 10 ) then
            attemps = attemps + 1;
            timer.performWithDelay( 2000, appodeal_show );
        end
    end
end]]--