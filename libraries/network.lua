--[[
local headers = {}
headers["Content-Type"] = "application/x-www-form-urlencoded"
headers["Accept-Language"] = "en-US"
]]--  

local function setUrlParams(data)
	local params = {};
	local tmp = "";
	if(type(data) == "table") then
		for k,v in pairs(data) do
			if(params == "") then
				tmp = k.."="..v;
			else
				tmp = (tmp.."&"..k.."="..v); 
			end	
		end	
		params.body = tmp;
	else
		params = "data = "..data; 	
	end	
	return params;
end	

function request(params,callback)
	if(type(params) == "table") then
		if(params.url ~= nil and params.url ~= "") then
			local url 		= params.url; 
			local method 	= (params.method ~= nil) and  params.method  			or "GET";
			local data 		= (params.data   ~= nil) and  setUrlParams(params.data) or {};
			local callback 	= (callback      ~= nil) and  callback      			or function(event) print(event.response) end;
			network.request( url, method, callback, data )
		else
			print("invalid url");
		end
	end	
end