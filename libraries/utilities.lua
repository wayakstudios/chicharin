function getJsonFF(name)
    local json = require( "json" )
    local filename = system.pathForFile("assets/files/json/"..name, system.ResourceDirectory )
    local decoded, pos, msg = json.decodeFile( filename )
     
    if not decoded then
        print( "Decode failed at "..tostring(pos)..": "..tostring(msg) )
    else
        print(decoded.key)
        --print_array( decoded,true );
    end
    return decoded;

end

function JSONdecode(data)
    --print(data);
    local json = require( "json" );
    local decoded, pos, msg = json.decode( data )
    if not decoded then
        print( "Decode failed at "..tostring(pos)..": "..tostring(msg) )
    else
        print( decoded.name )  --> 23.54
    end
    --print(decoded);
    --print_array(decoded,true);
    return decoded;

end 

function readFromFile(name)
    local path = system.pathForFile( name, system.ResourceDirectory )
    local file, errorString = io.open( path, "r" )
    local contents;
    if not file then
        -- Error occurred; output the cause
        print( "File error: " .. errorString )
    else
        -- Read data from file
        contents = file:read( "*a" )
        -- Output the file contents
        --print( "Contents of " .. path .. "\n" .. contents )
        -- Close the file handle
        io.close( file )
    end;
    file = nil
    return contents;
end

function getTableRandomValue(_table)
    return _table[math.random(1,table.getn(_table))];
end

function first(table)
    local tmp = {};
    if(type(table) == "table") then
        for k,v in pairs(table) do
            tmp = v;
            break;
        end
    end
    return tmp;
end

function validate(var,val)
    local tmp  = ((var ~= nil) and  var  or val);
    return tmp;
end

function table_merge( table1,table2 )
    for k,v in pairs(table2) do
        table.insert(table1, v);
    end;
    return table1;
end

function table_reindex( origin )
    local target = {};
    for k,v in pairs( origin ) do
        table.insert(target, v)
    end
    return target;
end

function print_array(array,recursive)
   if(type(array) == "table") then
        print("----------------------------------");
        for k,v in pairs(array) do
            if(type(v) == "table" and recursive == true) then
                print(k.."tabla");
                print_array(v);
            else
                if(type(v) == "table") then
                    print(k.." = Tabla");
                else
                    if(type(v) == "function" or type(v) == "userdata" ) then
                        print("function");
                    else
                       if(type(v) == "boolean") then
                            print (k..": boolean");
                       else
                            print (k..":"..v);
                        end    
                    end
                end
            end
        end
        print("----------------------------------");
    else
        print(array);
    end
end

function usleep(nMilliseconds)
    local nStartTime = system.getTimer()
    local nEndTime = nStartTime + nMilliseconds

    while true do
        if system.getTimer() >= nEndTime then
            break
        end
    end
 end

function incremement(var)
    var = var + 1;
    return var;
end 

function decrement(var)
    var = var - 1;
    return var;
end

function add(var,val)
    var = var + val;
    return var;
end

function substract(var,val)
    var = var - val;
    return var;
end

function count(table)
    local x = 0;
    for k,v in pairs(table) do
        x = x +1;
    end 
    return x;   
end


function getPercent( value,max )
    local tmp = value * 100;
    return tmp / max;
end

function getValuePercent( percent,max )
    return (percent*max)/100;
end

function getMatrix(x,y)
    local matrix = {};
    for tx=0,x,1 do
        matrix[tx] = {};
        for ty=0,y,1 do
            matrix[tx][ty] = {};
        end
    end

    return matrix;
end 

function getRandomNumber(min,max)
    local numbers = {};
    local options = {};
    local limiter = (5 * math.random(1,3));
    for i=1,limiter,1 do 
        numbers[i] = math.random(min,max); 
    end
    for i=1,4,1 do
        local key   = math.random(1,limiter);
        options[i]  = numbers[key]; 
    end
    local key = math.random(1,3);
    return  options[key]; 
end    

function getRandomPairs(min,max)
    local values = {};
    local v1 = math.random(min,max);
    local v2 = math.random(min,max);
    while v1 == v2 do
        v2 = math.random(min,max);
    end 
    values[0] = v1;
    values[1] = v2;
    return values;
end   

function getSizes()
    sizes      = {};
    sizes.w    = display.contentWidth;
    sizes.h    = display.contentHeight;
    sizes.cx   = display.contentCenterX;
    sizes.cy   = display.contentCenterY;
    sizes.ox   = display.screenOriginX;
    sizes.oy   = display.screenOriginY;
    sizes.so   = -640;
    sizes.sb   = 640;
    return sizes;
end

function getDU(unit)
    local content = 0;
    if(unit == "w") then
        content = display.contentWidth;
    elseif(unit == "h") then
        content =  display.contentHeight;
    elseif(unit == "cx") then
        content = display.contentCenterX;
    elseif(unit == "cy") then
        content = display.contentCenterY;
    elseif(unit == "oy") then
        content = display.screenOriginY;
    elseif(unit == "ox") then
        content = display.screenOriginX;
    end
    --print_array(content,true);
    return content;
end

function isset(var)
    local exist = false;
    if(var ~= nil) then
        exist = true;
    end
    return exist;
end 

function getHash( size )
    local words = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","q","r","s","t","u","v","w","x","y","z"};
    local key   = "";
    for i=1,size,1 do
        local _type = math.random(0,1);
        if(_type == 0) then
            key = key..(math.random(0,9));
        else
            local choice = math.random(0,1);
            key = (choice == 0) and key..(words[(math.random(1,table.getn(words)))]) or key..(string.upper(words[(math.random(1,table.getn(words)))])); 
        end
    end
    return key;
end   

function getNWE( string )
    local tmp  = "";
    for i in string.gmatch(string, ".") do
            if(i == ".") then
                break;
            end
            tmp = tmp..i;
        end
    return tmp;
end

function getOrientation( presice )
    local orientation = system.orientation;
    if(presice == nil) then
        if(orientation == "landscapeLeft" or orientation == "landscapeRight" ) then
            orientation = "landscape";
        end
        if(orientation == "portrait" or orientation == "portraitUpsideDown" ) then
            orientation = "portrait";
        end
    end
    print(orientation)
    return orientation;
end

function getEnviorment()
    return system.getInfo( "environment" );
end

function getDeviceInfo()
    local device = 
    {
        appName         = system.getInfo( "appName" ),
        manufacturer    = system.getInfo( "manufacturer" ),
        name            = system.getInfo( "name" ),
        platform        = system.getInfo( "platform" ),
        targetStore     = system.getInfo( "targetAppStore" ),
    };
    return device;
end

---------------[[FUNCIONES DE RED]]--------------

local function setUrlParams(data)
    local params = {};
    local tmp = "";
    if(type(data) == "table") then
        for k,v in pairs(data) do
            if(params == "") then
                tmp = k.."="..v;
            else
                tmp = (tmp.."&"..k.."="..v); 
            end 
        end 
        params.body = tmp;
    else
        params = "data = "..data;   
    end 
    return params;
end 

function request(params,callback)
    if(type(params) == "table") then
        if(params.url ~= nil and params.url ~= "") then
            local url       = params.url; 
            local method    = (params.method ~= nil) and  params.method             or "GET";
            local data      = (params.data   ~= nil) and  setUrlParams(params.data) or {};
            local callback  = (callback      ~= nil) and  callback                  or function(event) print(event.response) end;
            network.request( url, method, callback, data )
        else
            print("invalid url");
        end
    end 
end
