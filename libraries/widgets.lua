local utilities = require("libraries.utilities");
local sizes 	= getSizes();
local objs		= {};
local function draw(event)
	if ( event.phase == "began" ) then
    lastid = event.target.id;
    event.target:setFillColor(0.5,1,1);
    elseif event.phase == "moved" then
        if(event.target.id ~= lastid) then
            if(objs[lastid] ~= nil and event.target.x ~= nil) then
                local rect = display.newLine( objs[lastid].x,objs[lastid].y,event.target.x,event.target.y );
                rect:setStrokeColor( 1, 0, 0 )
                rect.strokeWidth = 8
            end    
             lastid = event.target.id;
        end
    elseif event.phase == "ended" then
       print("Termina");
    end    
end

function createMagicTable()
	local X = sizes.originX;
	local Y = sizes.originY;
	local space = sizes.width / 5;
	local tmpX = space;

	local id = 0;
	for i=1,4,1 do
		
		local tmpy = sizes.centerY+100;
		for x=1,5,1 do
			id = id +1;
	        objs[id] = {};
	        objs[id] = {};
			objs[id] =  display.newRect( tmpX, tmpy, 60, 60 );
			objs[id].id = id;
	        objs[id].ky = i;
	        objs[id].kx = x;
			objs[id]:addEventListener("touch",draw);
			tmpy = tmpy + 100;
		end
		tmpX = objs[id].x + space;
	end 
end	