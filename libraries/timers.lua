local _timers	= {};

function setTimer(id,time,_function,iterations )
	if(_timers[id] == nil) then
		iterations  = validate(iterations,1);
		_timers[id] = {};
		_timers[id] = timer.performWithDelay( time, _function,iterations );
	end
end

function getTimer( id )
	local tmp = {};
	if(_timers[id] ~= nil) then
		tmp = _timers[id];
	end
	return tmp;
end

function timerPause( id )

end

function timerResume( id )

end

function stopTimer( id )
	if(_timers[id] ~= nil) then
		timer.cancel(_timers[id]);
	end	
end

function simpleTimer(time,_function,iterations)
	iterations  = validate(iterations,1);
	local tmr 	= {};
	tmr 		= timer.performWithDelay( time, _function,iterations );	
	return tmr;
end

function delay(time,_function)
	timer.performWithDelay( time, _function, 1 );	
end