-------------------[[Librerias]]---------------------
local widget       = require( "widget" );
local utilities    = require( "libraries.utilities" );

-------------------[[Variables]]---------------------
local sizes        = getSizes();
local gameLayers   = {};      
local fonts        = {};
local groups       = {};
-------------------[[Funciones Privadas]]---------------------


-------------------[[Funciones Publicas]]---------------------

function setGroup(group,id)
    if(groups[id] == nil) then
        groups[id] = group;
    end
end

function toGroup(obj,id)
    if(groups[id]~=nil) then
        groups[id]:insert( obj )
        --print("insert into:"..id)
    end
end

function getLayer(layer)
    local l = {};
    if(gameLayers[layer] ~= nil) then
        l = gameLayers[layer];
    end
    return l;    
end

function getGroup(group)
    local g = {};
    if(groups[layer] ~= nil) then
        g = groups[group];
    end
    return g;    
end

function setFonts(path,font)
    for k,v in pairs(font) do 
        local tmp = getNWE(v);
        local url = path.."/"..v;
        fonts[tmp] = native.newFont( url );
    end
end

function getFont(font)
   local f = {};
   if(isset(fonts[font])) then 
        f = fonts[font];       
   else 
       print("not found: "..font);
        f = native.systemFont;
   end
   return f;
end

function getCircle(params)
    params.r        = validate(params.r,1);
    params.g        = validate(params.g,1);
    params.b        = validate(params.b,1);
    params.x        = validate(params.x,0);
    params.y        = validate(params.y,0);
    params.radio    = validate(params.radio,100);
    local circle    = display.newCircle( 0, 0, params.radio );
   circle:setFillColor( params.r,params.g,params.b )
    if(params.layer ~= nil) then
        toLayer(circle,params.layer);
    end
    if(params.alpha ~= nil) then
        circle.alpha = params.alpha;
    end
    if(params.name ~= nil) then
        circle.name = params.name;
    end
    circle.x = params.x;
    circle.y = params.y;
    return circle;
end

function getLine(params)
    params.s    = validate(params.s,0);
    params.e    = validate(params.e,sizes.h);
    params.r    = validate(params.r,1);
    params.g    = validate(params.g,1);
    params.b    = validate(params.b,1);
    params.s    = validate(params.s,1);
    params.x    = validate(params.x,0);
    params.y    = validate(params.y,0);
    local line  = display.newLine( params.s, params.e, 0, 0  );
    line:setStrokeColor(  params.r,  params.g, params.b  )
    line.strokeWidth =  params.s;

    if(params.layer ~= nil) then
        toLayer(line,params.layer);
    end
    line.x = params.x;
    line.y = params.y;
    --line:translate( params.x, params.y );
    print(line.x.." "..line.y)
    return line;
end

function getSimpleButton( params )
    local button      = {};  
    local data        = {};
    data.label        = (isset(params.text))      and params.text         or "";
    data.font         = (isset(params.font))      and getFont(params.font)or native.systemFont;
    data.onEvent      = (isset(params.event))     and params.event        or function() print("not_set")end ;
    data.labelColor   = (isset(params.color))     and params.color        or { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } }
    data.x            = (isset(params.x))         and params.x            or sizes.cx;
    data.y            = (isset(params.y))         and params.y            or sizes.cy;
    data.fontSize     = (isset(params.font_size)) and params.font_size    or 14;
    data.textOnly     = (isset(params.text_only)) and params.text_only    or true;
    data.emboss       = (isset(params.effect))    and params.effect       or false;
    data.labelAlign   = (isset(params.align))     and params.align        or "center";
    button             = widget.newButton(data);
   
    if(isset(params.layer)) then 
        toLayer(button,params.layer);
    end
    if(isset(params.group)) then 
        toGroup(button,params.group);
    end
    if(isset(params.value)) then
       button.value = params.value; 
    end
    --button.x = data.x;
    --button.y = data.y;
    button:translate( data.x, data.y );
    return button;
end


--[[    setLayers
    detalle:
        Crea las capas que se van a usar en el juego y las centra.
    parametros:
        layers  = [obligado](table)arreglo de capas 
                    ejemplo:{"back","main","front"}. 
    retorna:    (bool)true|false.
]]--
function setLayers(layers)
    local success    = false;
    gameLayers      = {};  
    if(type(layers) == "table") then
        for key,layer in pairs(layers) do
            gameLayers[layer]    = {};
            gameLayers[layer]    = display.newContainer( sizes.w,sizes.h   );
            gameLayers[layer]:translate( display.contentWidth*0.5, display.contentHeight*0.5 );
        end
        success = true;
    end 
    return success;   
end

--[[    toLayer
    detalle:
        agrega a un objeto a una capa ya creada.
    parametros:
        obj     = [obligado](table)el objeto a agregar a la capa.
        layer   = [obligado](string)la capa a la que se le agregara el objeto.
    retorna:    (bool)true|false.
]]--
function toLayer(obj,layer)
    local success = false;
    if(obj ~= nil) then
        if(gameLayers[layer] ~= nil) then
            gameLayers[layer]:insert( obj, true );
            success = true;
        else
            print("la capa "..layer.." no existe");
        end
    end
    return success;
end   

--[[    getSprite
    detalle:
        crea un sprite no animado, escalar o fijo.
    parametros:
        path    =[obligado](string) la ruta hacia la imagen.
        options =[opcional](table)  arreglo de opciones
            ejemplo:
            {
                r     = (bool)usar sprite escalar o normal, default no escalar.
                x     = (int) pocision en x, default 0.
                y     = (int) pocision en y, default 0.
                w     = (int) ancho de la imagen,default original.
                h     = (int) alto de la imagen,default original.
                layer = (string) capa en la que se agregara el sprite.
            }
    retorna:    (table)sprite.
]]--
function getSprite(path,options)
    local tmp   = {};
    local x     = (options.x ~= nil) and options.x or sizes.cx;
    local y     = (options.y ~= nil) and options.y or sizes.cy; 
    
    if(options.r == true) then
        tmp = display.newImageRect("assets/"..path,options.w,options.h);
    else
        tmp = display.newImage("assets/"..path);
    end
    if(isset(options.val)) then
        tmp.val = options.val;
    end
    if(isset(options.layer)) then
        toLayer(tmp,options.layer);
    end
    if(isset(options.group)) then
        toGroup(tmp,options.group);
    end
    if(isset(options.alpha)) then
        tmp.alpha = options.alpha;
    end
    if(options.name ~= nil) then
        tmp.name = options.name;
    end
    tmp.x = x;
    tmp.y = y;
    --if( tmp.x ~= nil ) then
       -- tmp:translate( x, y );
    --end
    return tmp;
end    

--[[    createPanel
    detalle:

    parametros:
]]--
function getPanel(options)
	r = (options.r 	~= nil) and  options.r  or 0;
	g = (options.g  ~= nil) and  options.g  or 0;
	b = (options.b 	~= nil) and  options.b 	or 0;
    x = (options.x 	~= nil) and  options.x 	or sizes.cx;
    y = (options.y 	~= nil) and  options.y 	or sizes.cy;
	
    local panel 	= {};
	panel 			= display.newRect(x,y,sizes.w,sizes.h);
    panel:setFillColor( r, g, b );
    if(options.layer ~= nil) then
        toLayer(panel,options.layer);
    end
    if(isset(options.group)) then
        toGroup(panel,options.group);
    end
    if(options.alpha ~= nil) then
        panel.alpha = options.alpha;
    end
    if(options.name ~= nil) then
        panel.name = options.name;
    end
    panel:translate( x, y ); 
    return panel;
end

function getRect(options)
    local r = (options['r']  ~= nil) and  options['r']  or 0;
    local g = (options['g']  ~= nil) and  options['g']  or 0;
    local b = (options['b']  ~= nil) and  options['b']  or 0;
    local h = (options['h']  ~= nil) and  options['h']  or sizes.w;
    local w = (options['w']  ~= nil) and  options['w']  or sizes.h;
    local x = (options['x']  ~= nil) and  options['x']  or 0;
    local y = (options['y']  ~= nil) and  options['y']  or 0;
    local panel     = {};
    panel           = display.newRect(x,y,w,h);
    panel:setFillColor( r, g, b );
    if(options.layer ~= nil) then
        toLayer(panel,options.layer);
    end
    if(options.group ~= nil) then
        toGroup(panel,options.group);
    end
    panel.x = x;
    panel.y = y;
    if(isset(options.alpha)) then
        panel.alpha = options.alpha;
    end
    if(isset(options.name)) then
        panel.name = options.name;
    end
   -- print("creado:"..x..":"..y);
    return panel;
end

function getText(txt,options)
   -- local font  = (options.font == nil) and native.systemFont or options.font;
    local opt   = 
    {
        text        = validate(txt,""),     
        x           = validate(options.x,sizes.cx),
        y           = validate(options.y,sizes.cy),
        width       = validate(options.w,sizes.w),
        --height      = 0;
        font        = getFont(options.font),--(options.font == nil) and native.systemFont or options.font, -- validate(getFont(options.font),native.systemFont),  
        fontSize    = validate(options.size,24),
        align       = validate(options.align,"center"),
        --anchorY = topAlignAxis;
    }
    options.x = validate(options.x,0);
    options.y = validate(options.y,0);
    options.r = validate(options.r,1);
    options.g = validate(options.g,1);
    options.b = validate(options.b,1);
    if(options.render == true) then
        tmp = display.newEmbossedText( opt );
    else
        tmp = display.newText( opt );
    end
    --local tmp = display.newText( opt );
    tmp:setFillColor( options.r, options.g, options.b );
    if(isset(options.layer)) then
        toLayer(tmp,options.layer);
    end
    if(options.group ~= nil) then
        toGroup(tmp,options.group);
    end
    tmp:translate( options.x, options.y );
    --tmp.x = validate(options.x,sizes.cx);
    --tmp.y = validate(options.y,sizes.cy);
    return tmp;
end 

local function getButtonData( data )
	local myClosure 		= function(event) if event.phase == "began" then print("hello world"); end;	end;
	local default  			= {};
   	default.label 			= (data.label 	~= nil) and  data.label 	or "Press";
    default.y 				= (data.X 		~= nil) and  data.X 		or 0;
    default.x 				= (data.Y 		~= nil) and  data.Y 		or 0;
    default.onEvent 		= (data.event 	~= nil) and  data.event 	or myClosure;
    default.shape 			= (data.shape 	~= nil) and  data.shape 	or "roundedRect";
    default.width 			= (data.width 	~= nil) and  data.width 	or 200;
    default.height 			= (data.height 	~= nil) and  data.height 	or 40;
    default.font 			= (data.font 	~= nil) and  data.font 		or "native.systemFont";
    default.cornerRadius 	= (data.radius 	~= nil) and  data.radius 	or 2;
    default.strokeWidth 	= (data.strokeW ~= nil) and  data.strokeW 	or 4;
    default.strokeColor 	= { default={0,0,0,}, over={0.8,0.8,1,1} };
    default.fillColor 		= { default={1,1,1,1}, over={1,0.1,0.7,0.4} };

    return default;
end

function getButton(data)
	local button 	= {}
    button 			= widget.newButton(getButtonData(data));  
    return button;
end	

function getAnimatedSprite( path,params )
    local sheetData     = { width=params.w, height=params.h, numFrames=params.frames}
    local sequenceData  =
    {
        name            = "normal",
        start           = 1,
        count           = params.frames,
        time            = params.time,
        loopCount       = validate(params.loop,0), 
        loopDirection   = validate(params.direction,"forward")
    }
    local imageSheet    = graphics.newImageSheet( "assets/"..path, sheetData ) 
    local sprite        = display.newSprite( imageSheet, sequenceData );

    sprite:setSequence( "normal" );
    if(params.layer ~= nil) then
        toLayer(sprite,params.layer);
    end
    if(params.name ~= nil) then
       sprite.name = params.name;
    end
    if(params.scale ~= nil) then
        sprite:scale(params.scale,params.scale);
    end
    sprite.x = validate(params.x,0);
    sprite.y = validate(params.y,0);
    if(params.play == true) then
        sprite:play();
    end
    return sprite;
end

function createImageFromSpriteSheet(width,height,frames,sprite_width,time,loop,folder,file,x,y,play)
    --local sprite    = nill;
    local options   =
    {
        width               = sprite_width,
        height              = height,
        numFrames           = frames,
        sheetContentWidth   = width,
        sheetContentHeight  = height
    }
    local sequences =
    {
        {
            name            = "normal",
            start           = 1,
            count           = frames,
            time            = time,
            loopCount       = loop,
            loopDirection   = "forward"
        }
    }
    --print("assets/"..folder.."/"..file..".png");
    local imageSheet    = graphics.newImageSheet( "assets/"..folder.."/"..file..".png", options );
    local sprite        = display.newSprite( imageSheet,sequences );
    sprite.x            = x;
    sprite.y            = y;
    sprite:setSequence( "normal" )  -- switch to "fastRun" sequence
    if(play == true) then
        sprite:play();
    end
     --printArray(sprite);
    return sprite;
end