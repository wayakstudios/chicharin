function vibrate()
    system.vibrate();
end

function getTimer()
	system.getTimer();
end	

function getOrientation()
	--[[
		"portrait"
		"landscapeLeft"
		"portraitUpsideDown"
		"landscapeRight"
	]]--
	return system.orientation;
end	