local lang          = "";
local dictionary    = {};

function setLang(l)
    lang  = l;
end

function loadLang(name)
    dictionary = require("langs."..name);
end

function getLTXT(key)
    txt = "";
    if(dictionary[key] ~= nil ) then
        txt = dictionary[key][lang];
    end
    return txt;
end

