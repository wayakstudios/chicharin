local facebook = require( "plugin.facebook.v4" );
 
local accessToken ="";

function connect() 
    if ( facebook.isActive ) then
        accessToken = facebook.getCurrentAccessToken()
        if ( accessToken ) then
             getText("existe",{layer="main",r=1,g=1,b=1});
            native.showAlert( "Facebook user already logged in", "User's access token: " .. accessToken.token )
        else
            getText("No existe",{layer="main",r=1,g=1,b=1});
            facebook.login()
           -- login();
        end
    end
end

-- Check for a value inside the provided table
local function valueInTable( t, valueToFind )
    for k,v in pairs( t ) do
        if v == valueToFind then
            return true
        end
    end
    return false
end
 
local function genericFacebookListener( event )
      getText("Logueado wii",{layer="main",r=1,g=1,b=1});
     y=-300;
   
   -- facebook.request( "me/feed", "POST", { message="Hello Facebook from CoronaSDK" } )
    for k,v in pairs(accessToken)do
         getText(k..":"..v,{layer="main",r=1,g=1,b=1,y=y});
         y = y +100;
    end
    --initfb();
end
 
-- This listener will handle the request of read-only permissions, then request publishable permissions
local function intermediateListener( event )
 
    if ( "session" == event.type ) then
        if ( "login" == event.phase ) then
            accessToken.token = facebook.getCurrentAccessToken()
 
            -- Continue only if the user granted the read-only permissions
            if ( valueInTable( accessToken.grantedPermissions, "user_events" ) ) then
                facebook.login( genericFacebookListener, { "user_events" } )
            else
                 getText("mno logueado",{layer="main",r=1,g=1,b=1});
                print( "The user did not grant the read-only permissions" )
            end
        end
    end
end

--connect();

function login() 
    -- Request read-only permissions, followed by publishable permissions
    facebook.login( intermediateListener, { "user_events" } )
end

local function shareLink( url )
 
    local accessToken = facebook.getCurrentAccessToken()
    if accessToken == nil then
        facebook.login()
    elseif not valueInTable( accessToken.grantedPermissions, "publish_actions" ) then
        facebook.login( { "publish_actions" } )
    else
        facebook.showDialog( "link", { link=url } )
    end
end
 
local function facebookListener( event )
 
    if ( "fbinit" == event.name ) then
 
        print( "Facebook initialized" )
 
        -- Initialization complete; share a link
        shareLink( "https://www.coronalabs.com/" )
 
    elseif ( "fbconnect" == event.name ) then
 
        if ( "session" == event.type ) then
            -- Handle login event and try to share the link again if needed
        elseif ( "dialog" == event.type ) then
            -- Handle dialog event
        end
    end
end
 
-- Set the "fbinit" listener to be triggered when initialization is complete
function initfb()
    facebook.init( facebookListener )
end

login();