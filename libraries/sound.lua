-------[[Librerias]]--

-------[[Variables]]-------

local sounds    = {};
local channels  = {};

-------[[Funciones]]-------

function load_sounds(path,table )
    for k,v in pairs(table) do
        local key   = getNWE(v);
        if(table.type == "audio") then
            sounds[key] = audio.loadStream( path.."/"..v );
        else 
            sounds[key] = audio.loadSound( path.."/"..v );
        end
    end
end

function play_sound( key,opts )
    opts = validate(opts,{});
    if(isset(sounds[key])) then
        local availableChn  = audio.findFreeChannel();
        opts.channel        = availableChn;
        channels[key]       = audio.play( sounds[key],opts);
    end
end

function stop_sound( key )
    if(isset(channels[key])) then
        audio.stop(  channels[key] )
        channels[key] = nil;
    end
end

function stop_sounds()
     audio.stop();
     for k,v in pairs( channels ) do
         channels[k] = nil;
     end
     channels = {};
end

function remove_sound( key )
    if(isset(sounds[key])) then
        sounds[key] = nil;
    end
end

function remove_sounds()
     for k,v in pairs( sounds ) do
         sounds[k] = nil;
     end
     sounds = {};
end