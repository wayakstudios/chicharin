--	Aqui Cargamos todas las Librerias de Corona a utilizar
local _lbs          = {};
_lbs['lng']         = require("libraries.lang");
_lbs['tmr']         = require("libraries.timers");
_lbs['utl']         = require("libraries.utilities");
_lbs['uix']         = require("libraries.ui");
_lbs['snd']         = require("libraries.sound");
_lbs['apd']         = require("libraries.appodeal");

--	Las capas que usaremos en el juego
setLayers({"back","props","main","misc","ui"});

-- Las canciones que utilizaremos en el juego
load_sounds("assets",
{
   "race.mp3",
   "mordida.wav",
   "life.wav",
   "combo.wav",
   "demage.wav",
   "caida.wav",
   "grito.wav"
});

-- Las fuentes
setFonts("assets",{"mining.otf"});

--play_sound("Arriba_Mami",{});


return _lbs;