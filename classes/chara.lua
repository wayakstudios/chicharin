local chara 	= {};
chara.__index 	= chara;

local function getChara()
	local sheetData1 	= { width=116, height=116, numFrames=20, sheetContentWidth=2320, sheetContentHeight=116 }
	local sheet1 		= graphics.newImageSheet( "assets/chicharo1.png", sheetData1 );
	local sheetData2 	= { width=116, height=116, numFrames=19, sheetContentWidth=2204, sheetContentHeight=116 }
	local sheet2		= graphics.newImageSheet( "assets/chicharo2.png", sheetData2 );
	local sequenceData 	= 
	{
        { name="seq1", sheet=sheet1, start=1, count=20, time=400, loopCount=1 },
        { name="seq2", sheet=sheet2, start=1, count=19, time=400, loopCount=1 },
    }
	local myAnimation = display.newSprite( sheet1, sequenceData );
   	toLayer(myAnimation,"main");
   	myAnimation.y = 0;
    return myAnimation;
end

function chara:create()
	local this = 
	{
		--obj 	= getAnimatedSprite("prota.png",{layer="main",x=0,y=0,frames=39,w=242,h=242,time=600,play=false,loop=1}),--getRect({layer="main",w=80,h=80,x=0,y=0,r=1,g=1,b=1}),
		obj 	= getChara();--getSprite("Burbuja.png",{r=true,layer="main",w=230,h=230,y=0,x=0}),
		objs 	= getSprite("Burbuja.png",{r=true,layer="main",w=230,h=230,y=0,x=0,alpha=0.8}),
		shield	= 1,
		status 	= 0,
		lifes 	= 3,
		blink 	= 0,
		anima 	= 1,
		pos 	= 0,
		isBig 	= 0,
		timer 	= {},
	};
	this.objs.rotation=-45
	--this.obj:scale(2,2);
	this.obj.name = "player"; 
	physics.addBody( this.obj, "dinamic",{ isSensor=false,radius=40 } );
	this.objs:toFront();
	setmetatable(this,chara);
	return this;
end

function chara:getX()
	return self.obj.x;
end

function chara:getY()
	return self.obj.y;
end

function chara:big()
	self.isBig =1;
	self.obj:scale(3,3);
end

function chara:normal()
	self.isBig =0;
	transition.to(self.obj,{time=300,xScale=2,yScale=2});
	--self.obj:scale(2,2);
end

function chara:Die()
	local closure 	= function()	
		self.obj.alpha 	= 0;
	end;
	simpleTimer(300,closure,1);
	self.anima 		= 0;
	
end

function chara:animate()
	local closure = function()
		if(self.anima == 1) then
			local tmp1 = getSprite("chicharo.png",{r=true,layer="main",w=80,h=80,y=self.obj.y,x=self.obj.x,alpha=0.3});
			if(self.isBig == 1) then
				tmp1:scale(3,3);
			else
				tmp1:scale(2,2);
			end
			
			self.obj:toFront();
			transition.to(tmp1,{time=300,y=tmp1.y-150,alpha=0.1,onComplete=function() display.remove(tmp1) end});
		end
	end;
	simpleTimer(20,closure,-1);
end

function chara:move( type )
	if(type=="l") then
		if(self.obj.x < - 200) then
			
			local closure = function()
				self.obj.x = 300;
				self.objs.x = 300;
				transition.to(self.objs,{time=25,x=250}); 
				transition.to(self.obj,{time=25,x=250}); 
			end;
			transition.to(self.objs,{time=25,x=-300}); 
			transition.to(self.obj,{time=25,x=-300,onComplete=closure}); 
		else
			transition.to(self.objs,{time=50,x=(self.objs.x - 250)}); 
			transition.to(self.obj,{time=50,x=(self.obj.x - 250)}); 
		end
	else
		if(self.obj.x >  200) then
			local closure = function()
				self.obj.x = -300;
				self.objs.x = -300;
				transition.to(self.objs,{time=25,x=-250}); 
				transition.to(self.obj,{time=25,x=-250}); 
			end;
			transition.to(self.objs,{time=25,x=300}); 
			transition.to(self.obj,{time=25,x=300,onComplete=closure}); 
			
		else
			transition.to(self.objs,{time=50,x=(self.objs.x + 250)}); 
			transition.to(self.obj,{time=50,x=(self.obj.x + 250)}); 
		end
		
	end

	print(self.obj.x)	
end

function chara:animation()
	self.obj:setSequence( "seq1" );
	self.obj:play();
	local closure = function()
		self.obj:setSequence( "seq2" );
		self.obj:play();
	end;
	simpleTimer(450,closure);
end

function chara:restart(  )
	--print("restart");
	local closure = function()
		self.obj.x 		= 0;
		self.obj.y 		= 0;
		self.objs.x 	= 0;
		self.objs.y 	= 0;
		self.status 	= 0;
		self.shield 	= 1;
		self.objs.alpha = 1;
		self.obj.alpha 	= 1;
		self.anima 		= 1;
		self.gmo 		= 0;
		self.lifes 		= 3;
		--physics.stop( );
	end
	simpleTimer(500,closure);
end

function chara:getLife()
	if(self.lifes < 3) then
		self.lifes = self.lifes +1;
		self.status = 1;
		self.obj.alpha=0.6;
		transition.to(self.obj,{time=800,y=(self.obj.y+130)});
		play_sound("life");
		local closure = function()
			self.status = 0;
			self.obj.alpha=1;
		end;
		simpleTimer(500,closure);
	end
end

function chara:damage()
	
	if(self.status == 0) then
		self:animation();
		transition.blink(self.obj,{time=100,tag="chara_demage"});
		local closure = function()
			transition.cancel("chara_demage");
			self.obj.alpha = 1;
		end;
		play_sound("demage");
		simpleTimer(300,closure);
		self.status = 1;
		if(self.shield == 1) then
			self.objs.alpha = 0;
			self.shield= 0.01;
		else
			--self.status = 1;
			self.lifes = self.lifes -1; 
			play_sound("grito");
			transition.to(self.obj,{time=100,y=(self.obj.y-120)});
			transition.to(self.objs,{time=100,y=(self.objs.y-120)});
			
		end
		local closure = function()
				self.status = 0;
		end;
		simpleTimer(1000,closure);
	end
end

function chara:destroy()

end

return chara;