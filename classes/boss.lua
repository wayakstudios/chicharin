local boss 		= {};
boss.__index 	= boss;

local function getBoss()
	-- 1st image sheet
	local sheetData1 	= { width=375, height=375, numFrames=10, sheetContentWidth=3750, sheetContentHeight=375 }
	local sheet1 		= graphics.newImageSheet( "assets/movimiento_1.png", sheetData1 )
	local sheetData2 	= { width=375, height=375, numFrames=10, sheetContentWidth=3750, sheetContentHeight=375 }
	local sheet2 		= graphics.newImageSheet( "assets/movimiento_2.png", sheetData1 )
	local sheetData3 	= { width=375, height=375, numFrames=10, sheetContentWidth=3750, sheetContentHeight=375 }
	local sheet3 		= graphics.newImageSheet( "assets/movimiento_3.png", sheetData1 )
	local sheetData4 	= { width=375, height=375, numFrames=10, sheetContentWidth=3750, sheetContentHeight=375 }
	local sheet4 		= graphics.newImageSheet( "assets/movimiento_4.png", sheetData1 )
	local sequenceData 	= 
	{
        { name="seq1", sheet=sheet1, start=1, count=10, time=250, loopCount=1 },
        { name="seq2", sheet=sheet2, start=1, count=10, time=250, loopCount=1 },
        { name="seq3", sheet=sheet3, start=1, count=10, time=250, loopCount=1 },
        { name="seq4", sheet=sheet4, start=1, count=10, time=250, loopCount=1 }
    }
    local myAnimation = display.newSprite( sheet1, sequenceData );
   	toLayer(myAnimation,"misc");
   	myAnimation.y = -300
    return myAnimation;
end

local function getTongue()
	local sheetData1 	= { width=142, height=126, numFrames=21, sheetContentWidth=2982, sheetContentHeight=126 }
	local sheet1 		= graphics.newImageSheet( "assets/lengua1.png", sheetData1 );
	local sheetData2 	= { width=142, height=126, numFrames=21, sheetContentWidth=2982, sheetContentHeight=126 }
	local sheet2		= graphics.newImageSheet( "assets/lengua2.png", sheetData2 );
	local sequenceData 	= 
	{
        { name="seq1", sheet=sheet1, start=1, count=21, time=400, loopCount=1 },
        { name="seq2", sheet=sheet2, start=1, count=21, time=400, loopCount=1 },
    }
	local myAnimation = display.newSprite( sheet1, sequenceData );
   	toLayer(myAnimation,"misc");
   	myAnimation.y = -550
    return myAnimation;
end

local function getBite()
	-- 1st image sheet
	local sheetData1 	= { width=375, height=375, numFrames=10, sheetContentWidth=3750, sheetContentHeight=375 }
	local sheet1 		= graphics.newImageSheet( "assets/mordida1.png", sheetData1 )
	local sheetData2 	= { width=375, height=375, numFrames=10, sheetContentWidth=3750, sheetContentHeight=375 }
	local sheet2 		= graphics.newImageSheet( "assets/mordida2.png", sheetData1 )
	local sequenceData 	= 
	{
        { name="seq1", sheet=sheet1, start=1, count=10, time=400, loopCount=1 },
        { name="seq2", sheet=sheet2, start=1, count=9, time=400, loopCount=1 }
    }
    local myAnimation = display.newSprite( sheet1, sequenceData );
   	toLayer(myAnimation,"misc");
   	myAnimation.y = -300
    return myAnimation;
end 

function boss:create()
	local this = 
	{
		_lengua 	= getTongue();
		_obj		= getBoss(),
		_atack		= getBite(),
		_colider	= getRect({layer="misc",h=50,w=700,x=0,y=-400,r=1,alpha=0.02}),
		_timer 		= {},
		_atimer 	= {},
		_status 	= 0,
		_flag		= 0,
		_current	= 0,
		_tongue		= 0,
	};
	this._atack.alpha = 0;
	this._obj:scale(2,2);
	this._lengua:scale(2,2);
	this._atack:scale(2,2);
	this._colider.name		= "monster";
	physics.addBody( this._colider, "static",{ isSensor=true } );	
	setmetatable(this,boss);
	return this;
end

function boss:animation()
	local closure = function()
		self._current = self._current + 1;
		if(self._current > 4) then
			self._current = 1;
		end 
		if(self._tongue == 0) then
			self._tongue = 1;
			self._lengua:setSequence( "seq1" );
       		self._lengua:play();
		else
			self._tongue = 0;
			self._lengua:setSequence( "seq2" );
       		self._lengua:play();
		end
		self._obj:setSequence( "seq"..self._current );
        self._obj:play() 
	end
	self._atimer = simpleTimer(255,closure,-1);
end

function boss:animate()
	self._obj:play();
	local closure = function()
		if(self._flag ==0) then
			self._flag = 1;
			--self._obj:scale(1.01,1.01);
			transition.to(self._obj,{time=1000,y= self._obj.y - 100})
			--self._obj.y= self._obj.y - 100;
		else
			self._flag = 0;
			--self._obj:scale(0.99,0.99);
			transition.to(self._obj,{time=1000,y= self._obj.y + 100})
		end	
	end
	local closure2 = function ( )
		local closure3 = function()
			self._obj.y = -600;
		end;
		transition.to(self._obj,{time=1000,y=-700,onComplete=closure3});
	end
	simpleTimer(1000,closure,-1);
	--simpleTimer(800,closure2,-1);
	--closure2();
end

function boss:restart()
	--self._lengua.y 		= -500;
	--self._lengua.alpha 	= 1;
	self._obj.alpha 	= 1;
	self._atack.alpha 	= 0;
	
end

function boss:atack()
	--transition.to(self._lengua,{time=500,y=self._lengua.y-300});
	self._lengua.alpha 	= 0;
	self._obj.alpha 	= 0;
	self._atack.alpha 	= 1;
	self._atack:setSequence( "seq1" );
	self._atack:play();
	play_sound("mordida");
	local closure = function()
		self._atack:setSequence( "seq2" );
		self._atack:play();
	end;
	simpleTimer(450,closure);
end

function boss:destroy()

end
return boss;