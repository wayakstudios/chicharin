local _square 		= require("classes.square");  
local _obstacle 	= require("classes.obstacle");  

local spawner 		= {};
spawner.__index 	= spawner;

function spawner:create()
	local this =
	{
		_current 	= 0, 
		_currentX 	= 0, 
		_currentY 	= 0, 
		_status		= 0,
		_objs		= {},
		_obst		= {},
		_timer 		= {}
	};
	setmetatable(this,spawner);
	return this;
end

function spawner:generate(_type,position)
	self._current = self._current +1;
	self._objs[self._current] =_square:create(_type,position);
	self._objs[self._current]:setID(self._current);
	self._objs[self._current]:move();
end

function spawner:start()
	local two 			= {1,0,0,0,1,0,0,0,0,1}
	local position 		= {250,0,-250};
	--0 = nada, caja para combo
	--1 = obstaculo simple
	--2 = obstaculo que se desplaza a lo largo de la pantalla de izq a der
	--3 = obstaculo que se desplaza a lo largo de la pantalla de der a iz
	--4 = obstaculo que va y viene de su posicion
	local combinations	= 
	{
		{0,0,1},
		{1,0,0},
		{0,1,0},
		{0,1,1},
		{1,1,0},
		{1,0,1},
		{2,0,0},
		{0,0,3},
		{1,0,4},
		{5,0,1}
	};
	local positions		= 
	{
		-250,0,250
	}
	if(self._status == 0) then
		local closure = function (  )
			local tmp 					= combinations[math.random(1,table.getn(combinations))];
			self._currentY 				= self._currentY +1;
			self._objs[self._currentY] 	= {};
			for k,v in pairs(tmp) do
				self._current 								= self._current +1;
				self._currentX 								= self._currentX +1;
				self._objs[self._currentY][self._currentX] 	= {};
				self._objs[self._currentY][self._currentX] 	=_square:create(positions[k],k);
				self._objs[self._currentY][self._currentX]:setID(self._current,self._currentY,self._currentX);
				self._objs[self._currentY][self._currentX]:move();
				
				if(v > 0) then
					self._obst[self._current] 	= _obstacle:create(v,positions[k],k);
					self._obst[self._current]:setID(self._current,self._currentY,self._currentX);
					self._obst[self._current]:move();
					self._obst[self._current]:traslate(self._objs);
				end

			end
			self._currentX 	= 0;
		end
		self._timer = timer.performWithDelay( 800, closure,-1 );
	end
end

function spawner:activate( y,x )
	self._objs[y][x]:activate();
end

function spawner:deactivate( y,x )
	self._objs[y][x]:deactivate();
end

function spawner:remove(posy,posx)
	self._objs[posy][posx]:remove();
end

function spawner:stop()
	for k,v in pairs(self._objs) do
		for kv,vv in pairs(v) do
			vv:destroy();
		end
	end
	for k,v in pairs(self._obst) do
		v:destroy();
	end
	self._current 	= 1;
	self._objs 		= {};
	timer.cancel(self._timer);
end

function spawner:destroy()

end

return spawner;