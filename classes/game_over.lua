local frame 	= {};
frame.__index 	= frame;

function frame:Create( value,max )
	local this = 
	{
		bkg 	= getRect({layer="ui",w=550,h=400,x=20,y=20,r=0,g=0,b=0,alpha=0.2}),
		frame 	= getRect({layer="ui",w=550,h=400,x=0,y=0,r=1,g=1,b=1}),
		top		= getRect({layer="ui",w=550,h=100,x=0,y=-150,r=0,g=0,b=0,alpha=1}),
		tittle 	= getText("Game Over!",{r=1,g=1,b=1,font="mining",size=60,layer="ui",x=0,y=-140}),
		score 	= getText("Score",{r=0,g=0,b=0,font="mining",size=60,layer="ui",x=0,y=-60}),
		value 	= getText(value,{r=1,g=0,b=0,font="mining",size=100,layer="ui",x=0,y=30}),
		max 	= getText("Record:"..max,{r=0,g=0,b=0,font="mining",size=60,layer="ui",x=0,y=120}),
	}
	setmetatable(this,frame);
	return this;
end

function frame:Destroy()
	for k,v in pairs(self) do
		display.remove(v);
	end
	self = nil;
end

return frame;	