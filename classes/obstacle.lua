local square 	= {};
square.__index 	= square;

local obstacles = 
{
	--"Brocoli.png",
	--"Hueso_pez.png",
	"Pollo.png",
	"Zanahoria.png"
}
local outlines = 
{
	["Pollo.png"] 		= graphics.newOutline( 0.1, "assets/Pollo.png" ),
	["Zanahoria.png"]	= graphics.newOutline( 0.1, "assets/Zanahoria.png" ),
}

function square:create(_type, position, order )
	
	local speed 	= {2500};
	local sprite 	= obstacles[math.random(1,table.getn(obstacles))];
	local this 		= 
	{
		_obj	= {},
		id 		= 0,
		posy 	= 0,
		posx 	= 0,
		type	= _type,
		position= order,
		current = order,                                                                                                                       order,
		sprite 	= sprite,
		speed	= speed[math.random(1,table.getn(speed))],
	};
	if(_type > 0) then
		this._obj		= getSprite(sprite,{layer="main",r=true,w=200,h=150,x=position,y=600});
		this._obj.name 	= "obstacle";
		physics.addBody( this._obj, "dinamic",{ outline=outlines[sprite],isSensor=true } ); 
		this._obj.rotation=80;
	end
	setmetatable(this,square);
	return this;
end

function square:setID( id,posy,posx )
	self.id 		= id;
	self._obj.id 	= id;
	self.posy 		= posy;
	self._obj.posy 	= posy;
	self.posx 		= posx;
	self._obj.posx 	= posx;
end

function square:move(  )
	local position 	= {250,0,-250};
	local closure 	= function()
		self:destroy();
	end;
	transition.to(self._obj,{time=self.speed,y=-600,onComplete=closure});
	--[[
	if(self.type == 2) then
		local closure = function(event)
			local times = {0,250}
			
			counts = event.count;
			local closure2 = function()
				self:deactivate();
				self.position = self.position+1;
				transition.to(self._obj,{time=200,xScale=1,yScale=1,x=(times[event.count])});
			end;
			transition.to(self._obj,{time=100,rotation=180,xScale=1.3,yScale=1.3,onComplete=closure2}); 
		end;
		simpleTimer(500,closure,2)
	end	

	elseif(self.type == 3) then
		local closure = function(event)
			local times = {0,-250}
			counts = event.count;
			local closure2 = function()
				transition.to(self._obj,{time=300,xScale=1,yScale=1,x=(times[event.count])});
			end;
			transition.to(self._obj,{time=200,rotation=180,xScale=1.3,yScale=1.3,onComplete=closure2});
		end;
		simpleTimer(600,closure,math.random(1,2))	

	elseif(self.type == 4) then
		if(self._obj.x == 250) then
		local closure = function(event)
			local times = {0,250}
			counts = event.count;
			local closure2 = function()
				transition.to(self._obj,{time=300,xScale=1,yScale=1,x=(times[event.count])});
			end;
			transition.to(self._obj,{time=200,rotation=180,xScale=1.3,yScale=1.3,onComplete=closure2});
		end;
		simpleTimer(600,closure,math.random(6))
		elseif(self._obj.x == -250) then
		local closure = function(event)
			local times = {0,-250}
			counts = event.count;
			local closure2 = function()
				transition.to(self._obj,{time=300,xScale=1,yScale=1,x=(times[event.count])});
			end;
			transition.to(self._obj,{time=200,rotation=180,xScale=1.3,yScale=1.3,onComplete=closure2});
		end;
		simpleTimer(600,closure,math.random(6))
		end
	end]]--
end


function square:animateRound(objects)
	local times = {0,250}
	local closure = function()
		objects[self.posy][self.posx]:deactivate();
		self.posx = self.posx +1;
		objects[self.posy][self.posx]:activate();
		transition.to(self._obj,{time=200,xScale=1,yScale=1,x=(times[self.position])});
		self.position = self.position + 1;
	end
	transition.to(self._obj,{time=200,rotation=180,xScale=1.3,yScale=1.3,onComplete=closure});
end

function square:animateRL(objects)
	local times = {0,-250,0}
	local closure = function()
		objects[self.posy][self.posx]:deactivate();
		self.posx = self.posx -1;
		objects[self.posy][self.posx]:activate();
		transition.to(self._obj,{time=200,xScale=1,yScale=1,x=(times[self.position])});
		self.position = self.position - 1;
	end
	if(self._obj~=nil) then
		transition.to(self._obj,{time=200,rotation=((self._obj.rotation ~= nil) and self._obj.rotation+45 or 45 ),xScale=1.3,yScale=1.3,onComplete=closure});
	end	
end

function square:animateLR(objects)
	local times = {0,250}
	local closure = function()
		objects[self.posy][self.posx]:deactivate();
		self.posx = self.posx +1;
		objects[self.posy][self.posx]:activate();
		transition.to(self._obj,{time=300,xScale=1,yScale=1,x=(times[self.position])});
		self.position = self.position + 1;
	end
	if(self._obj~=nil) then
		transition.to(self._obj,{time=200,rotation=((self._obj.rotation ~= nil) and self._obj.rotation+45 or 45 ),xScale=1.3,yScale=1.3,onComplete=closure});
	end	
end

function square:traslate( objects )
	local closure;
	local times = 0;
	if(self.type == 2) then
		self._obj:setFillColor(0.5,0,0);
		times = 2;
		closure = function (  )
			self:animateLR(objects);
		end
	elseif(self.type == 3) then
		self._obj:setFillColor(0.5,0,0);
		times = 2;
		closure = function (  )
			self:animateRL(objects);
		end
	elseif(self.type == 4) then

	end
	simpleTimer(800,closure,times)
end

function square:remove()
	self:destroy();
end

function square:destroy(  )
	display.remove(self._obj);
	self = nil;
end

return square;