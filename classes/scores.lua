local sql  		= require("libraries.sql");
local db 		= "game";

local score 	= {};
score.__index 	= score;

function score:create(  )
	connectDB(db);
	local data = getData("game_data");
	closeDB();
	local this = 
	{
		max_score  = (data[1] ~= nil) and data[1].score or 0,
	};
	setmetatable(this,score);
	return this;
end

function score:update()
	connectDB(db);
	local data = getData("game_data");
	closeDB();
	self.max_score  = (data[1] ~= nil) and data[1].score or 0;
	return self.max_score;
end

function score:get(  )
	return self.max_score;
end

function score:set( score )
	connectDB(db);
	updateValue("game_data","score",score);
	closeDB();
end

function score:destroy(  )
	self = nil;
end

return score;