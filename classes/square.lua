local square 	= {};
square.__index 	= square;


function square:create( position, order )
	
	local speed 	= {2500};
	local this 		= 
	{
		_back	= getRect({layer="main",w=150,h=150,x=position,y=600,r=0,g=0,b=0,alpha = 0.01}),
		id 		= 0,
		posy 	= 0,
		posx 	= 0,
		position= order,
		speed	= speed[math.random(1,table.getn(speed))],
	};
	this._back.name 	= "clear";
	this._back.status 	= 0;  
	physics.addBody( this._back, "dinamic",{ isSensor=true } );
	setmetatable(this,square);
	return this;
end

function square:setID( id,posy,posx )
	self.id 		= id;
	self._back.id 	= id;
	self._back.posy = posy;
	self._back.posx = posx;
end

function square:activate( )
	--self._back:setFillColor(1,1,1)
	self._back.status 	= 1;
end

function square:deactivate( )
	if(self._back ~= nil ) then
		--self._back:setFillColor(0,0,0)
		self._back.status 	= 0;
	end
end

function square:move( )
	local closure 	= function()
		self:destroy();
	end;
	transition.to(self._back,{time=self.speed,y=-600,onComplete=closure});
end

function square:remove()
	self:destroy();
end

function square:destroy(  )
	display.remove(self._back);
	self = nil;
end

return square;