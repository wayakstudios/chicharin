local menu 		= {};
menu.__index 	= menu;

function menu:create()
	local this = 
	{
		panel	= getPanel({layer="ui",r=1,g=1,b=1}),
	};
	setmetatable(this,menu);
	return this;
end

function menu:destroy()
	display.remove(self.panel);
	self = nil;
end

return menu;
