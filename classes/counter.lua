local Counter 		= {};
Counter.__index 	= Counter;

function Counter:Create()
	local this = 
	{
		value 	= 0,
		status 	= 0,
		bkg		= getRect({layer='ui',w=sizes.w,h=220,x=0,y=530,alpha=0.4}),
		obj 	= getText("000",{font="mining",size=100,layer="ui",x=0,y=480}),
		timer 	= {},
	};
	setmetatable(this,Counter);
	return this;
end

function Counter:Get()
	return self.value;
end

function Counter:Update()
	self.value = self.value + 10;
	local text = self.value;
	if(self.value < 100 ) then
		text = "0"..text;
	end
	self.obj.text = text;
end

function Counter:Start()
	if(self.status == 0) then
		self.status = 1;
		local closure = function()
			self:Update();
		end;
		self.timer = timer.performWithDelay(1000,closure,-1);
	end
end

function Counter:Stop()
	if(self.status == 1) then
		self.status= 0;
		self.timer = timer.cancel(self.timer);
		self:Restart();
	end	
end

function Counter:Restart()
	self.value 		= 0;
	self.obj.text 	= "000";
end

function Counter:Destroy()

end

return Counter;