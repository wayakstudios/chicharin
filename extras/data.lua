local crypto 	= require( "crypto" )
local sql  		= require("libraries.sql");
local db 		= "game";

local function setDefaults()
	local tablesetup 	= "CREATE TABLE IF NOT EXISTS game_data(id INTEGER PRIMARY KEY autoincrement, hash TEXT, token TEXT, apikey TEXT, score INTEGER, rank INTEGER, created TEXT, updated TEXT);";
	executeQuery(tablesetup);
	local tablesetup 	= "CREATE TABLE IF NOT EXISTS game_ranks(id INTEGER PRIMARY KEY autoincrement, player TEXT, score INTEGER, rank INTEGER, created TEXT, updated TEXT);";
	executeQuery(tablesetup);
	local deviceData 	= getDeviceInfo();
	local hash 			= deviceData.name.."|"..deviceData.platform.."|"..deviceData.appName.."|"..deviceData.manufacturer;
	hash 				= crypto.digest( crypto.sha1, hash );
	token				= getHash(24);
	local defaultData	= "INSERT INTO game_data(hash,token,apikey,score,rank,created,updated) VALUES('"..hash.."','"..token.."','',0,0,date(),date());"; 
	executeQuery(defaultData);
end

local function testDB()
	connectDB(db);
	local query = "SELECT * FROM game_data";
	local resu  = executeQuery(query);
	if(resu == 1) then
			setDefaults();
			--print_array(getData("game_data"),true);
		else
			print("Todo bien capitan");	
			--print_array(getData("game_data"),true);
		end	
	closeDB();	
	return resu;
end

function initialize_storage( reset )
	if(reset) then
		print("borrando_contenido");
		connectDB(db);
		droptable("game_data");
		closeDB();
	end
	if (pcall(testDB)==1) then
   		
	else
		
	end
end