function getMonster(params)
    local tmp 		= {};
	local options 	= 
	{
	  	sourceWidth			= 375,
	  	sourceHeight		= 15575,
	  	width 				= 375,
		height 				= 375,
		numFrames 			= 41,
		sheetContentWidth 	= 15575,  -- width of original 1x size of entire sheet
    	sheetContentHeight 	= 375
	};
	local sheet 		= graphics.newImageSheet( "assets/monster_move.png", options )
	local sequenceData 	= 
	{
		{ name = "normal", frames={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, time=800, loopCount=0 },
	}
			 
	tmp = display.newSprite( sheet, sequenceData );
	tmp:setSequence( "normal" );
	tmp:play();

    if(isset(params.layer))then
        toLayer(tmp,params.layer);
    end
	
	tmp.x 		    = params.x;
	tmp.y	 	    = params.y;
	local Closure   = function(event)
        if(event.phase == "ended") then
           -- display.remove(tmp)
        end
    end
    tmp:addEventListener( "sprite", Closure )
    return tmp;
end