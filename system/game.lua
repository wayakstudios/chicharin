local physics 	= require("physics");
local __player 	= require("classes.chara");
local __spawner = require("classes.spawner");
local __boss 	= require("classes.boss");
local __counter = require("classes.counter");
local __gameover= require("classes.game_over");
local __scores	= require("classes.scores");

physics.start( );
physics.setDrawMode( "normal" ) 
physics.setGravity( 0, 0 )

local player 	= __player:create();
player:animate();
local spawner 	= __spawner:create();
local scores 	= __scores:create();
fondo				= {};
fondo['uno']		= getSprite("BG.png",{layer="back",x=0,y=0,h=sizes.h,w=sizes.w});
fondo['dos']		= getSprite("BG.png",{layer="back",x=0,y=(1665),h=sizes.h,w=sizes.w});
fondo['uno'].number	= 0;
fondo['dos'].number	= 1665;
local controls	= {};
local _menu		= {};
local _text		= {};
local monster 	= __boss:create();
local counter 	= __counter:Create();
monster:animate();
monster:animation();
local gm 		= {};
local start_ 	= 0;

local combo 	= 0;
local comboY 	= 0;	
local _games	= 0;

function bakg()
	local closure = function()
		fondo['uno'].number  = fondo['uno'].number + 5;
		fondo['dos'].number  = fondo['dos'].number + 5;
		fondo['uno'].y = fondo['uno'].y - 5;
		fondo['dos'].y = fondo['dos'].y - 5;
		if(fondo['uno'].number == 1665) then
			fondo['uno'].y 		= 0;
			fondo['uno'].number = 0;
		end
		if(fondo['dos'].number == 3330) then
			fondo['dos'].y 		= 1665;
			fondo['dos'].number = 1665;
		end
	end
	simpleTimer(50,closure,-1);
end

function player_control( event )
	if(event.target.type == "l") then
		player:move("l");
	else
		player:move("r");
	end;
end

function swipe_control(event)
			
	if event.phase == "began" then

	elseif event.phase == "moved" then

	elseif event.phase == "ended" then
		if(event.x > event.xStart) then
			print("derecha");
			player:move("r");
		else
			print("izquierda")
			player:move("l");
		end
	end
end

function start_game()
	play_sound("caida");
	combo 	= 0;
	comboY 	= 0
	display.remove(_menu)
	display.remove(__bar)
	display.remove(_text);
	display.remove(_ltap);
	display.remove(_rtap);
	display.remove(_face);
	_menu = {};
	scores:update();
	setControls();
	local closure = function()
		spawner:start();
	end;
	simpleTimer(2000,closure,1);
	player:normal();
	counter:Start();
	if(gm.frame ~= nil) then
		gm:Destroy();
	end
	start_ = true;
	--monster:atack();
	_icon 	= getSprite("tapTick.png",{layer="ui",r=true,y=200,x=250,w=100,h=100});
	--transition.blink(_icon,{time=600});
	local i	= 0;
	local closure = function(event)
		if(event.count == 4) then
			display.remove(_icon);
		else
			if(i == 0 ) then
				i =1;
				--_icon.x = -250;
				transition.to(_icon,{time=400,x=-250});
			else
				i =0;
				--_icon.x = 250;
				transition.to(_icon,{time=400,x=250});
			end
		end
	end;
	simpleTimer(700,closure,4);

end

function menu()
	showAdds();
	print(scores:get());
	_menu 	= getPanel({layer="ui",x=0,y=-200,r=1,g=1,alpha=0.01});
	__bar	= getRect({layer="props",w=sizes.w,h=150,x=0,y=300,r=0,g=0,b=0,alpha=0.8});
	_text	= getText("TAP TO PLAY",{font="mining",size=60,layer="ui",x=0,y=300});
	_ltap 	= getSprite("tapLeft.png",{layer="main",r=true,y=300,x=-250,w=150,h=80});
	_rtap 	= getSprite("tapRight.png",{layer="main",r=true,y=300,x=250,w=150,h=80});
	_face	= getSprite("face.png",{layer="ui",x=250,r=true,y=490,h=120,w=120});
	_menu:addEventListener("tap",start_game);
	_face:addEventListener("tap",face_link);
	player:big();
end

function initialize()
	player_control();
end

function restart_game()
	appodeal_hide();
	monster:restart();
	player:restart();
	menu();
end

function game_over()
	
	monster:atack();
	start_ = false;
	spawner:stop();
	removeControls();
	local score = counter:Get();
	counter:Stop();
	if(score>scores:get()) then
		scores:set(score);
	end
	player:Die();
	local closure = function()
		gm = __gameover:Create(score,scores:get());
		restart_game();
	end;
	simpleTimer(1000,closure,1);

end

function removeControls()
	display.remove(controls["l"]);
	--display.remove(controls["r"]);
end

function setControls()
	controls["l"] = getRect({layer="main",l="main",r=1,g=1,b=1,x=0,y=0,h=sizes.h,w=(sizes.w),alpha=0.05});
	--[[controls["l"] = getRect({layer="main",l="main",r=1,g=0,b=1,x=-190,y=0,h=sizes.h,w=(sizes.w/2),alpha=0.01});
	controls["r"] = getRect({layer="main",l="main",r=1,g=1,b=0,x=190,y=0,h=sizes.h,w=(sizes.w/2),alpha=0.01});
	controls["l"].type = "l";
	controls["r"].type = "r";

	controls["l"]:addEventListener("tap",player_control);
	controls["r"]:addEventListener("tap",player_control);]]--
	controls["l"]:addEventListener("touch",swipe_control);

end

function showAdds()
	_games = _games+1;
	if(_games%3 == 0) then
		appodeal_setAdd(3);
	else
		appodeal_setAdd(2);
	end;
	appodeal_show();
end

function iscombo()
	local text =  getText("X"..combo,{font="mining",size=80,layer="misc",x=player:getX(),y=player:getY()});
	play_sound("combo");
	local closure = function()
		display.remove(text);
	end;
	transition.to(text,{time=600,y=text.y-150,onComplete=closure});
end

function face_link()
	system.openURL( "https://www.facebook.com/SateliteGameStudio/es" );
end

local function onGlobalCollision( event )
    if(event.object1.name == "monster" or event.object2.name == "monster") then
    	  --print(event.object1.name);
    	   -- print(event.object2.name);
    end
    --print(event.object2.name);
    if ( event.phase == "began" ) then
        if(event.object2.name == "player" and event.object1.name == "monster") then
        	game_over();
        	--player:damage();
        end
     	 if(event.object2.name == "monster" and event.object1.name == "player") then
        	game_over();
        	--player:damage();
        end
        if(event.object1.name == "player" and event.object2.name == "obstacle") then
        	combo = 0;
        	player:damage();
        end
        if(event.object2.name == "player" and event.object1.name == "obstacle") then
        	combo = 0;
        	player:damage();
        end

        if(event.object1.name == "monster" and event.object2.name == "obstacle") then
        	spawner:remove(event.object2.posy,event.object2.posx)
        	--event.object2:remove(event.object2.id);
        end
        if(event.object2.name == "monster" and event.object1.name == "obstacle") then
        	--event.object1:remove();
        end

        --[[if(event.object2.name == "clear" and event.object1.name == "obstacle") then
        	spawner:activate(event.object2.posy,event.object2.posx);
        end]]--
        
        if(event.object2.name == "player" and event.object1.name == "clear") then
        	if(event.object1.status == 0) then
        		if(comboY ~= event.object1.posy) then
	        		comboY = event.object1.posy;
	        		combo = combo +1;
	        		if(combo%10 == 0) then
	        			iscombo();
	        			player:getLife();
	        		end
	        	end
        	end
        end
        
        --print( "began: " .. event.object1.name .. " and " .. event.object2.name )
 
    elseif ( event.phase == "ended" ) then
       --[[ if(event.object2.name == "clear" and event.object1.name == "obstacle") then
        	spawner:deactivate(event.object2.posy,event.object2.posx);
        end]]--
        --print( "ended: " .. event.object1.name .. " and " .. event.object2.name )
    end
end
menu();
bakg();
play_sound("race",{ loops = -1});

local last = 0;
local function onAccelerate( event )
     if start_ == true then
	    local extra = 20 ;
	    
	    if(event.xGravity > 0 ) then
	    	if(last == 1) then
	    		last = 0;
	    		--extra = extra + 20;
	    	end
	    	if(player.obj.x < 400 ) then
		    	transition.to(player.obj,{time=80,x=player.obj.x + extra })
		    else
		    	player.obj.x = -400;
		    end
	    else
	    	if(last == 0) then
	    		last = 1;
	    		--extra = extra + 20;
	    	end
	    	if(player.obj.x > -400 ) then
		    	transition.to(player.obj,{time=80,x=player.obj.x - extra })
		    else
		    	player.obj.x = 400;
		    end
	    	--transition.to(player.obj,{time=80,x=player.obj.x -60})
	    end
	    --_X.text = "gravity : "..event.xGravity;
	end
    
end
  
Runtime:addEventListener( "collision", onGlobalCollision )